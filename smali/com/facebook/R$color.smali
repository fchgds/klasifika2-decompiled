.class public final Lcom/facebook/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final cardview_dark_background:I = 0x7f070012

.field public static final cardview_light_background:I = 0x7f070013

.field public static final cardview_shadow_end_color:I = 0x7f070014

.field public static final cardview_shadow_start_color:I = 0x7f070015

.field public static final com_facebook_blue:I = 0x7f070016

.field public static final com_facebook_button_background_color:I = 0x7f070017

.field public static final com_facebook_button_background_color_disabled:I = 0x7f070018

.field public static final com_facebook_button_background_color_focused:I = 0x7f070019

.field public static final com_facebook_button_background_color_focused_disabled:I = 0x7f07001a

.field public static final com_facebook_button_background_color_pressed:I = 0x7f07001b

.field public static final com_facebook_button_background_color_selected:I = 0x7f07001c

.field public static final com_facebook_button_border_color_focused:I = 0x7f07001d

.field public static final com_facebook_button_login_silver_background_color:I = 0x7f07001e

.field public static final com_facebook_button_login_silver_background_color_pressed:I = 0x7f07001f

.field public static final com_facebook_button_send_background_color:I = 0x7f070020

.field public static final com_facebook_button_send_background_color_pressed:I = 0x7f070021

.field public static final com_facebook_button_text_color:I = 0x7f070066

.field public static final com_facebook_device_auth_text:I = 0x7f070022

.field public static final com_facebook_likeboxcountview_border_color:I = 0x7f070023

.field public static final com_facebook_likeboxcountview_text_color:I = 0x7f070024

.field public static final com_facebook_likeview_text_color:I = 0x7f070025

.field public static final com_facebook_messenger_blue:I = 0x7f070026

.field public static final com_facebook_send_button_text_color:I = 0x7f070067

.field public static final com_facebook_share_button_text_color:I = 0x7f070027


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
