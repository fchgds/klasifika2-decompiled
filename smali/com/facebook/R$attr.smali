.class public final Lcom/facebook/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final cardBackgroundColor:I = 0x7f01009e

.field public static final cardCornerRadius:I = 0x7f01009f

.field public static final cardElevation:I = 0x7f0100a0

.field public static final cardMaxElevation:I = 0x7f0100a1

.field public static final cardPreventCornerOverlap:I = 0x7f0100a3

.field public static final cardUseCompatPadding:I = 0x7f0100a2

.field public static final com_facebook_auxiliary_view_position:I = 0x7f0100f3

.field public static final com_facebook_confirm_logout:I = 0x7f0100f5

.field public static final com_facebook_foreground_color:I = 0x7f0100ef

.field public static final com_facebook_horizontal_alignment:I = 0x7f0100f4

.field public static final com_facebook_is_cropped:I = 0x7f0100fa

.field public static final com_facebook_login_text:I = 0x7f0100f6

.field public static final com_facebook_logout_text:I = 0x7f0100f7

.field public static final com_facebook_object_id:I = 0x7f0100f0

.field public static final com_facebook_object_type:I = 0x7f0100f1

.field public static final com_facebook_preset_size:I = 0x7f0100f9

.field public static final com_facebook_style:I = 0x7f0100f2

.field public static final com_facebook_tooltip_mode:I = 0x7f0100f8

.field public static final contentPadding:I = 0x7f0100a4

.field public static final contentPaddingBottom:I = 0x7f0100a8

.field public static final contentPaddingLeft:I = 0x7f0100a5

.field public static final contentPaddingRight:I = 0x7f0100a6

.field public static final contentPaddingTop:I = 0x7f0100a7


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
