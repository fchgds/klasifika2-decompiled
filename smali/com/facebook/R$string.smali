.class public final Lcom/facebook/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final com_facebook_device_auth_instructions:I = 0x7f0b0021

.field public static final com_facebook_image_download_unknown_error:I = 0x7f0b0022

.field public static final com_facebook_internet_permission_error_message:I = 0x7f0b0023

.field public static final com_facebook_internet_permission_error_title:I = 0x7f0b0024

.field public static final com_facebook_like_button_liked:I = 0x7f0b0025

.field public static final com_facebook_like_button_not_liked:I = 0x7f0b0026

.field public static final com_facebook_loading:I = 0x7f0b0027

.field public static final com_facebook_loginview_cancel_action:I = 0x7f0b0028

.field public static final com_facebook_loginview_log_in_button:I = 0x7f0b0029

.field public static final com_facebook_loginview_log_in_button_long:I = 0x7f0b002a

.field public static final com_facebook_loginview_log_out_action:I = 0x7f0b002b

.field public static final com_facebook_loginview_log_out_button:I = 0x7f0b002c

.field public static final com_facebook_loginview_logged_in_as:I = 0x7f0b002d

.field public static final com_facebook_loginview_logged_in_using_facebook:I = 0x7f0b002e

.field public static final com_facebook_send_button_text:I = 0x7f0b002f

.field public static final com_facebook_share_button_text:I = 0x7f0b0030

.field public static final com_facebook_tooltip_default:I = 0x7f0b0031

.field public static final messenger_send_button_text:I = 0x7f0b0035


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
